R__LOAD_LIBRARY(single_rate/librates)
	
	//load the external libraries
extern "C"{
	// (particle for wiser only, 1 for pi+;  2 for pi-, 3=k+, 4=k-, 5=p) (target  0 for hydrogen  1 for deuterium) (ebeam MeV) (pmin MeV) (pstep MeV) (pmax MeV)  (thmin deg) (thstep deg) (thmax deg) (XS nb/GeV/sr) (success flag) 
   void wiser_(int *,int *,float *,float *,float *,float *,float *,float *, float *, float *, int *);
   void whitlow_(int *,float *,float *,float *, float *, float *,float *,float *,float *, int *);
   //beame, momentum, theta, target, XS, flag
   void qfsrad_(float *,float *,float *, int *, float *, int *);
}

//important variables to set!

//cuts for integration over acceptance, NOT THE GENERATION LIMITS
double dpc = 0.1;  //the cut (+/-) on dp in percent (1.0 = 100%)
double thc = 0.06; //the cut (+/-) on theta (out of plane) in rad
double phc = 0.04; //the cut (+/-) on phi (in plane) in rad
int N = 1e6;  //number of events to loop through for integration over acceptance

//GENERATION LIMIT CUTS, read below..
//generation limit with respect to central angle input...  i.e. if centang = 15deg, and thcut = 3.8, then the generation will happen over 15 +/- 3.8
double thcut = 3.8;

//generation limit for +/- phi azimuthal in lab-frame.  NOTE:  This is calculated below, so does not need to be changed here.
double phicut = 35.0;

//experimental parameters
float current = 50e-6; //A
float current2 = current/1.60217662e-19; //e/s
double targlen = 10.0; //cm
double targden = 0.071; //g/cm3
double A = 1.00782505; //hygrogen molar mass;
double Ntg = targlen*targden*TMath::Na()/A;
double cm2_nbarn = 1e-33;
double lum = current2*Ntg*cm2_nbarn;


TRandom3 *rand3 = new TRandom3(678687); //random random seed
double D2R = TMath::DegToRad();

//this function does all the calls and returns the rates. 
void retXS(float beamE, float p, float th, float *e1xs, float *e2xs, float *pxs, float *pimxs, float *pipxs, int pol = -1){
	int targ = 0;
	float stepwhit = 0.0;
	float stepwis = 0.1;
	int flag = 0;
	int ptype = 0;
	
	*e1xs = 0.0;
	*e2xs = 0.0;
	*pipxs = 0.0;
	*pimxs = 0.0;
	*pxs = 0.0;
	
	if(pol < 0){ //negatively charged polarity rates (e- and pi-minus)
		whitlow_(&targ, &beamE, &p, &stepwhit, &p, &th, &stepwhit, &th, e1xs, &flag);
		if(flag) *e1xs = 0.0;
		qfsrad_(&beamE, &p, &th, &targ, e2xs, &flag);
		if(flag) *e2xs = 0.0;
		ptype = 2;
		wiser_(&ptype, &targ, &beamE, &p, &stepwis, &p, &th, &stepwis, &th, pimxs, &flag);
		if(flag) *pimxs = 0.0;
	}else{ //positively charged polarity rates (p and pi-plus)
		ptype = 1;
		wiser_(&ptype, &targ, &beamE, &p, &stepwis, &p, &th, &stepwis, &th, pipxs, &flag);
		if(flag) *pipxs = 0.0;
		ptype = 5;
		wiser_(&ptype, &targ, &beamE, &p, &stepwis, &p, &th, &stepwis, &th, pxs, &flag);
		if(flag) *pxs = 0.0;
	}
}

void calcRates(vector<float> *vrates, double beame, double centp, double centang, int pol = -1){
	/*
	//hack for SHMS having a larger dp acceptance
	if(pol < 0){
		dpc = 0.25;
	}
	*/
	
	//make phicut large-enough to cover all acceptance:
	double smallest_theta = centang*D2R - sqrt(thc*thc + phc*phc);
	phicut = phc/sin(smallest_theta)/D2R;
	if(phicut > 180.0 || phicut < 0) phicut = 180.0;

	double cthmin = cos((centang + thcut)*D2R);
	double cthmax = cos((centang - thcut)*D2R);
	int ninside = 0;
        
	//double solidAngGen = (cthmax - cthmin)*2.0*phicut*TMath::Pi();
        
	//TH2F *hthph = new TH2F("hthph","",100,-thc, thc, 100, -phc, phc);
	float totXSe1 = 0;
	float totXSe2 = 0;
	float totXSp = 0;
	float totXSpim = 0;
	float totXSpip = 0;
	
	float thcutmax = 0;
	float phcutmax = 0;
	
	for(int i = 0; i < N; i++){
		double cth = rand3->Uniform(cthmin, cthmax);
		double ph = rand3->Uniform(-phicut, phicut);
		double p = rand3->Uniform((1.0 - dpc)*centp, (1.0 + dpc)*centp);
		float th = acos(cth)/D2R;
					
		float e1xs, e2xs, pxs, pimxs, pipxs;
					
		retXS(beame*1000.0, p*1000.0, th, &e1xs, &e2xs, &pxs, &pimxs, &pipxs, pol);
		totXSe1 += e1xs*sin(th*D2R);
		totXSe2 += e2xs*sin(th*D2R);
		totXSp += pxs*sin(th*D2R);
		totXSpim += pimxs*sin(th*D2R);
		totXSpip += pipxs*sin(th*D2R);
	                
		//cout << p << endl;
                
		TVector3 vp;
		vp.SetMagThetaPhi(p,acos(cth),ph*D2R);
		vp.RotateY(-centang*D2R);
		vp.RotateZ(TMath::Pi());
                
		double specth = vp.X()/vp.Z();
		double specph = vp.Y()/vp.Z();
		double dp = (p - centp)/centp;
                
		if(fabs(dp) < dpc && fabs(specth) < thc && fabs(specph) < phc){
			//cout << specth << "  " << specph << "  " << dp << endl;
			ninside++;
			//hthph->Fill(specth, specph);
			if(fabs(acos(cth)/D2R - centang) > thcutmax) thcutmax = fabs(acos(cth)/D2R - centang);
			if(fabs(ph) > phcutmax) phcutmax = fabs(ph);
                        
		}
	}
	
	
	
	
	cout << "beame: " << beame << "  cent-ang: " << centang << "  cent-p: " << centp << "  magnet polarity: " << pol << endl;
	cout << "  p-gen range (GeV): " << (1.0 - dpc)*centp << " to " << (1.0 + dpc)*centp << ",  theta-gen range (deg): " << centang - thcutmax << " to " << centang + thcutmax << ",  phi-gen range (deg): +/-" << phcutmax << "  number of counts inside acceptance: " << ninside << endl;
	
	if(pol < 0){
		//cout << "  e- (whitlow): " << (float)ninside/(float)N/(float)N*totXSe1*(cthmax - cthmin)*(2.0*phicut*D2R)*(centp*2.0*dpc)*lum*1e-3 << endl;
		//cout << "  e- (qfs radiated): " << (float)ninside/(float)N/(float)N*totXSe2*(cthmax - cthmin)*(2.0*phicut*D2R)*(centp*2.0*dpc)*lum*1e-3 << endl;
		//cout << "  pi- (wiser): " << (float)ninside/(float)N/(float)N*totXSpim*(cthmax - cthmin)*(2.0*phicut*D2R)*(centp*2.0*dpc)*lum*1e-3 << endl;
		
		float erate1 = (float)ninside/(float)N/(float)N*totXSe1*(cthmax - cthmin)*(2.0*phicut*D2R)*(centp*2.0*dpc)*lum*1e-3;  //whitlow kHz
		float erate2 = (float)ninside/(float)N/(float)N*totXSe2*(cthmax - cthmin)*(2.0*phicut*D2R)*(centp*2.0*dpc)*lum*1e-3;  //qfs kHz
		//cout << erate1 << "  " << erate2 << endl;
		float erate = erate1;
		if(erate2 > erate1) erate = erate2;
		float pimrate = (float)ninside/(float)N/(float)N*totXSpim*(cthmax - cthmin)*(2.0*phicut*D2R)*(centp*2.0*dpc)*lum*1e-3; //wiser pi-minus kHz
		//cout << erate << "  " << pimrate << endl;
		vrates->push_back(erate);
		vrates->push_back(pimrate);
		
	}else{
		//cout << "  p (wiser): " << (float)ninside/(float)N/(float)N*totXSp*(cthmax - cthmin)*(2.0*phicut*D2R)*(centp*2.0*dpc)*lum*1e-3 << endl;
		//cout << "  pi+ (wiser): " << (float)ninside/(float)N/(float)N*totXSpip*(cthmax - cthmin)*(2.0*phicut*D2R)*(centp*2.0*dpc)*lum*1e-3 << endl;
		
		float prate = (float)ninside/(float)N/(float)N*totXSp*(cthmax - cthmin)*(2.0*phicut*D2R)*(centp*2.0*dpc)*lum*1e-3; //wiser proton
		float piprate = (float)ninside/(float)N/(float)N*totXSpip*(cthmax - cthmin)*(2.0*phicut*D2R)*(centp*2.0*dpc)*lum*1e-3; //wiser pi-plus
		
		vrates->push_back(prate);
		vrates->push_back(piprate);
	}
	
	//*vrates = retv;
	
	//hthph->Draw("colz");
	
	
	
	
	
	
}