you must build the library in the directory single_rates first (type scons)

You can edit the beam energy, multiple central momenta, and multiple angles all spectrometer settings in calcAll.C
You can edit the experiment specific variables (current, target, theta/phi/dp acceptance of spectrometers) in calcRates.h
